<!DOCTYPE html>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
     <link href="/CSS/booknow.css" type="text/css" rel="stylesheet" />
    <title>BOOKING_page</title>
  </head>
  <body>
    <div class="full-page">
      <div class="form-box">
        <form id="booking" class="bookingpage">
          <h1 class="head">Normal Booking</h1>
          <br />

          <span class="inp">Train Number :</span
          ><select name="Train Number" id="stationname" class="input-field">
          
          <option value="train">select trainno</option>
          <c:forEach var="ts" items="${Trainlist}">       
            <option value="10001">${ts.trainid}</option>      
            </c:forEach>
          </select>

          <span class="inp">From Station :</span>
          <select name="From" id="stationname" class="input-field">
          <option value="station">Select Station</option>
            <option value="ANDHERI">ANDHERI</option>
            <option value="BANDRA">BANDRA</option>
            <option value="BOTIVALI">BOTIVALI</option>
            <option value="CHARNI RD">CHARNI RD</option>
            <option value="DADAR">DADAR</option>
            
          </select>

          <span class="inp">To Station :</span
          ><select name="To" id="stationname" class="input-field">
          <option value="station">Select Station</option>
           <option value="ANDHERI">ANDHERI</option>
            <option value="BANDRA">BANDRA</option>
            <option value="BOTIVALI">BOTIVALI</option>
            <option value="CHARNI RD">CHARNI RD</option>
            <option value="DADAR">DADAR</option>
          </select>

          <button type="submit" class="booknow" onclick="book()">Book Now</button>
        </form>
      </div>
    </div>
    
    
    <h1 id="book"></h1>
    <script type="text/javascript">

    function book(){
         
       alert("TICKET BOOKED SUCCESFULLY");
         }
    
    </script>
  </body>
</html>