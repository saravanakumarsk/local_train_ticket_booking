<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    
    <style>
body {
  font-family: Arial, Helvetica, sans-serif;
  background-color: lightblue;
  padding:25px;
  
}

.title{
  color:#5C6AC4;
  }
* {
  box-sizing: border-box;
}

/* Style inputs */
input[type=text], select, textarea {
  width: 100%;
  padding: 12px;
  border: 1px solid #ccc;
  margin-top: 6px;
  margin-bottom: 16px;
  resize: vertical;
}

input[type=submit] {
  background-color: #04AA6D;
  color: white;
  padding: 12px 20px;
  border: none;
  cursor: pointer;
}

input[type=submit]:hover {
  background-color: #45a049;
}

/* Style the container/contact section */
.container {
  border-radius: 5px;
  background-color: #f2f2f2;
  padding: 10px;
}

/* Create two columns that float next to eachother */
.column {
  float: left;
  width: 50%;
  margin-top: 6px;
  padding: 20px;
}

/* Clear floats after the columns */
.row:after {
  content: "";
  display: table;
  clear: both;
}

/* Responsive layout - when the screen is less than 600px wide, make the two columns stack on top of each other instead of next to each other */
@media screen and (max-width: 600px) {
  .column, input[type=submit] {
    width: 100%;
    margin-top: 0;
  }
}
</style>
</head>
<body>


<div class="container">
  <div style="text-align:center">
    <h2>Contact Us</h2>
    <p>If u have any Queries,Please feel free to talk with us!</p>
  </div>
  </div>
  <div>
<div>
  
  
  <p style="color:green;">
    
<u>Station Name</u> : Andheri</div>
<p style="color:blue;"><u>Address</u> : Railway Colony, Andheri East, Mumbai, Maharashtra 400053</p>
<p><u>No.of Platforms</u> : 3</p>

<p><u>Phone No</u> : 022-243543</p>
<p><u>E-mail</u> : andheri@indianrail.gov.in</p>
</div>

&nbsp;

<div>
  
  <p style="color:green;">
<u>Station Name</u> : Bandra</div>
<p style="color:blue;"><u>Address</u> : Bairam Naupada, Bandra East, Mumbai, Maharashtra 400050</p>
<p><u>No.of Platforms</u> : 4</p>

<p><u>Phone No</u> : 022-254321</p>
<p><u>E-mail</u> : bandra@indianrail.gov.in</p>

</div>
&nbsp;
<div>
  
  <p style="color:green;">
<u>Station Name</u> : Botivali</div>
<p style="color:blue;"><u>Address</u> : Chinchpada, Borivali, Mumbai, Maharashtra 400066</p>
<p><u>No.of Platforms</u> : 3</p>

<p><u>Phone No</u> : 022-265432</p>
<p><u>E-mail</u> : Botivali@indianrail.gov.in</p>

</div>
&nbsp;
<div>
  
  <p style="color:green;">
<u>Station Name</u> : Charni Rd</div>
<p style="color:blue;"><u>Address</u> : Maharshi Karve Road, Girgaon, Mumbai, Maharashtra 400004</p>
<p><u>No.of Platforms</u> : 3</p>

<p><u>Phone No</u> : 022-265454</p>
<p><u>E-mail</u> : charniroad@indianrail.gov.in</p>
</div>
&nbsp;
<div>
  
  <p style="color:green;">
<u>Station Name</u> : Dadar</div>
<p style="color:blue;"><u>Address</u> : Dadar East, Dadar, Mumbai, Maharashtra 400014</p>
<p><u>No.of Platforms</u> : 8</p>

<p><u>Phone No</u> : 022-265432</p>
<p><u>E-mail</u> : dadar@indianrail.gov.in</p>
</div>
&nbsp;
</body>
</html>