<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
 
     
    <title>train ticket booking login</title>
    
         <link href="/CSS/fro.css" type="text/css" rel="stylesheet" />
  </head>
  <body>
    <div class="full-page">
      <div id="login-form" class="login-page">
        <div class="form-box">
          <div class="button-box">
            <div id="btn"></div>
            <button
              type="button"
              onclick="login()"
              id="loginbtn"
              class="toggle-btn"
            >
              LOGIN
            </button>
            <button type="button" onclick="register()" class="toggle-btn">
              REGISTER
            </button>
          </div>
          
          <form id="login" class="input-group-login" method="POST" action="/user/login/dashboard">
           
            <input type="email" class="input-field" id="email" name="email" placeholder="User Name" required />
                        
            <input type="password" class="input-field" id="password" name="password" placeholder="Enter Password" required
             /> 
                       
            <input type="checkbox" class="check-box" /><span
              >&ensp;Remember Password</span
            >
            <button type="submit" id="loginpagebtn" class="submit-btn">
              SIGN IN
            </button>
          </form>
          
          
          <form id="register" class="input-group-register" action="/user/login/save" method="post">
           
           <input type="text" class="input-field" id="Id" name="Id" placeholder="userid" required
             /> 
              
                      
            <input type="text" class="input-field" id="firstName"  name="firstName" placeholder="First Name"
              required
            />
                         
              
            <input type="text" class="input-field" id="lastname" name="lastname" placeholder="Last Name"
               required
            />
                           
            <input type="email" class="input-field" id="email" name="email" placeholder="Email Id"
               required
            />
                         
            <input type="password" class="input-field" id="password" name="password" placeholder="Enter Password"             
              required
            />
           
            <input
              type="checkbox"
              class="check-box"
              id="register-checkbox"
            /><span>&ensp;I agree to the terms and conditions</span>
            <button type="submit" class="submit-btn">SIGN UP</button>
         
         
          </form>
        </div>
      </div>
    </div>
    <script>
      var x = document.getElementById("login");
      var y = document.getElementById("register");
      var z = document.getElementById("btn");
      function register() {
        x.style.left = "-450px";
        y.style.left = "50px";
        z.style.left = "110px";
      }
      function login() {
        x.style.left = "50px";
        y.style.left = "450px";
        z.style.left = "0px";
      }
    </script>
    <script>
      var modal = documet.getElementById("login-form");
      window.onclick = function (event) {
        if (event.target == modal) {
          modal.style.display = "none";
        }
      };
    </script>
  </body>
</html>