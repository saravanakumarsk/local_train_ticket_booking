<!DOCTYPE html>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
         <link href="/CSS/trainlist.css" type="text/css" rel="stylesheet" />
  <title>Train List</title>
  </head>
  <body>
    <div><h1>Train List</h1></div>
    
    <table border="3">
    <thead>
      <tr>      
        <th>TRAIN NUMBER</th>
        <th>ANDHERI</th>
        <th>BANDRA</th>
        <th>BOTIVALI</th>
        <th>CHARNI RD</th>
        <th>DADAR</th>
        <th>DATE</th>
      </tr>
      </thead>
      <tbody>
      <c:forEach var="ts" items="${Trainstatus}">
      <tr>
        <td>${ts.trainid}</td>
        <td>${ts.station1}</td>
        <td>${ts.station2}</td>
        <td>${ts.station3}</td>
        <td>${ts.station4}</td>
        <td>${ts.station5}</td>
        <td>${ts.d_a_t_e}</td>        
      </tr>
      </c:forEach>
      </tbody>     
    </table>
    
  </body>
</html>