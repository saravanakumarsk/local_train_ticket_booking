<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    
    <link href="/CSS/cancelnow.css" type="text/css" rel="stylesheet" />
    
    <title>Cancel_page</title>
    <script type="text/javascript">

    function book(){
         
       alert("TICKET CANCELLED SUCCESFULLY");
         }
    
    </script>
  </head>
  <body>
    <div class="full-page">
      <div class="form-box">
        <form id="cancelling" class="cancellationpage">
          <h1 class="head">Ticket Cancellation </h1>

          <input
            type="text"
            id="ticketnum"
            class="input-field"
            placeholder="Ticket Number"
          />
           
          <button type="submit" onclick="book()" class="Cancellation">
            Cancel Ticket
          </button>
        </form>
        
      </div>
    </div>
    
    <h1 id="book"></h1>
    
  </body>
</html>