package com.example.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.example.entity.Booking;
import com.example.entity.Passenger;

public interface BookingRepository extends CrudRepository<Booking,Integer>{
	
	public Booking deleteById(int ticketno);
	public List<Booking> findAll();

}
