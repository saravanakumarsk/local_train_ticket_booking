package com.example.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.web.servlet.ModelAndView;


import com.example.entity.Passenger;


@Repository
public interface PassengerRepository extends CrudRepository<Passenger,Integer> {

	public List<Passenger> findAll();
	
	public Optional<Passenger> findById(Integer userid);
	
	@Query(value="SELECT email,password FROM Passenger  WHERE email=:email and password:password",nativeQuery=true)
    public boolean findByEmail(@Param("email") String email, @Param("password")String password);
	
	@Query("SELECT p FROM Passenger p WHERE p.email= (:email)")
	Passenger validatePassenger(@Param("email") String email);
    
	public Passenger save(Passenger passenger);
}