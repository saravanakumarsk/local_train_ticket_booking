package com.example.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="booking")
public class Booking {
     @Id
     @Column(name="ticketno")
	 private int ticketno;
	
     @Column(name="fromstation")
	private String fromstation;
	
     @Column(name="tostation")
	private String tostation;
     
     public Booking() {}

	public Booking(int ticketno, String fromstation, String tostation) {
		super();
		this.ticketno = ticketno;
		this.fromstation = fromstation;
		this.tostation = tostation;
	}

	public int getTicketno() {
		return ticketno;
	}

	public void setTicketno(int ticketno) {
		this.ticketno = ticketno;
	}

	public String getFromstation() {
		return fromstation;
	}

	public void setFromstation(String fromstation) {
		this.fromstation = fromstation;
	}

	public String getTostation() {
		return tostation;
	}

	public void setTostation(String tostation) {
		this.tostation = tostation;
	}

	
     
	
}
