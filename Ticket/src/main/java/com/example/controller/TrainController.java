package com.example.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;
import org.springframework.http.MediaType;

import com.example.entity.Booking;
import com.example.entity.Train;
import com.example.repository.BookingRepository;
import com.example.repository.TrainRepository;

@RestController
public class TrainController {
	
	@Autowired
	TrainRepository trainRepository;
	
	@Autowired
	BookingRepository bookingRepository;

	
	//for contact us page
	@GetMapping(value="/dashboard/contact")
	public ModelAndView ContactUsPage(ModelAndView mv) {
		
	mv.setViewName("contactus");
	return mv;
	}
	
	@GetMapping(value="/dashboard/wallet")
	public ModelAndView WalletPage(ModelAndView mv) {
		
	mv.setViewName("ewallet");
	return mv;
	}
	
	//for ticket booking page
		@GetMapping(value="/dashboard/book")
		public ModelAndView BookingPage() {
			
			List<Train> trainlist=trainRepository.findAll();
			
			ModelAndView mv=new ModelAndView("booknow","Trainlist",trainlist);
			return mv;
		}
	
	
		//for ticket cancel page
		@GetMapping(value="/dashboard/cancel")
		public ModelAndView CancelPage(ModelAndView mv) {
			
		mv.setViewName("cancelnow");
		return mv;
		
		}
		
		//for list of bookings page
		
		@GetMapping(value="/dashboard/bookinglist")
		public ModelAndView Bookingspage() {
			
			List<Booking> bookinglist=bookingRepository.findAll();
			
			ModelAndView mv=new ModelAndView("bookinglist","BookingList",bookinglist);
			return mv;
		}
		
		

		@RequestMapping("/delete/{ticketno}")
	    public RedirectView deleteDoctor(@PathVariable(name = "ticketno") int ticketno, HttpServletRequest request)
	    {
			bookingRepository.deleteById(ticketno);
	    	RedirectView redirectView = new RedirectView();
	    	redirectView.setUrl(request.getContextPath() + "/dashboard/bookinglist");
	    	return redirectView;
	    }
		
		
		
	    //for train status page
	     @GetMapping(value="/dashboard/status")
	     public ModelAndView statuspage() {
		
		List<Train> trainstatus=trainRepository.findAll();
		
		ModelAndView mv=new ModelAndView("trainlist","Trainstatus",trainstatus);
		return mv;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	   
}
